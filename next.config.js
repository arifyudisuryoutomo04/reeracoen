/** @type {import('next').NextConfig} */
module.exports = {
    env: {
        customKey: process.env.MOST_API_KEY
    },
    reactStrictMode: true,
    swcMinify: true
};

// const nextConfig = {
//     reactStrictMode: true,
//     swcMinify: true
// };

// module.exports = nextConfig;