/* eslint-disable react-hooks/rules-of-hooks */
import type { NextPage } from "next";
import Hero from "../components/Hero";
import React, { useState, useEffect } from "react";
import {
  Box,
  Heading,
  Link,
  Image,
  Text,
  HStack,
  Tag,
  useColorModeValue,
  Container,
  SimpleGrid,
  Button,
  Center
} from "@chakra-ui/react";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import { decrement, increment } from "../counter/counterSlice";
import { RootState } from "../store";
import InfiniteScroll from "react-infinite-scroll-component";

const Home: NextPage = ({ Most: initialMost }) => {
  const count = useSelector((state: RootState) => state.counter.value);
  const dispatch = useDispatch();
  const [next, setNext] = useState([1 && 7 && 30]);
  const [Most, setMost] = useState([]);

  async function loadMore() {
    const req = await fetch(
      `https://api.nytimes.com/svc/mostpopular/v2/emailed/${next}.json?api-key=${process.env.customKey}`
    );
    const Most = await req.json();
    setNext(next);
    console.log(Most);
  }

  useEffect(() => {
    async function fetchMost() {
      const res = await axios.get(
        `https://api.nytimes.com/svc/mostpopular/v2/emailed/${next}.json?api-key=${process.env.customKey}`
      );
      setMost(res.data.results);
    }
    fetchMost();
  }, []);

  return (
    <>
      <Hero />

      <Container maxW={"7xl"} p="12">
        <Heading
          as="h1"
          style={{
            marginBottom: "3%"
          }}
        >
          Most Popular
        </Heading>
        <InfiniteScroll
          dataLength={Most.length}
          // next={fetchMore}
          // hasMore={hasMore}
          loader={<h4>Loading...</h4>}
        >
          <SimpleGrid columns={{ base: 1, md: 2 }} spacing={10}>
            {Most &&
              Most.map((e, index) => {
                const {
                  title,
                  abstract,
                  byline,
                  published_date,
                  subsection,
                  id,
                  media
                } = e;
                return (
                  <Box
                    key={index + 1}
                    marginTop={{ base: "1", sm: "5" }}
                    display="flex"
                    flexDirection={{ base: "column", sm: "row" }}
                    justifyContent="space-between"
                  >
                    <Box
                      display="flex"
                      flex="1"
                      marginRight="3"
                      position="relative"
                      alignItems="center"
                    >
                      <Box
                        width={{ base: "100%", sm: "85%" }}
                        zIndex="2"
                        marginLeft={{ base: "0", sm: "5%" }}
                        marginTop="5%"
                      >
                        <Link
                          textDecoration="none"
                          _hover={{ textDecoration: "none" }}
                        >
                          {media &&
                            media.map((e, index) => {
                              const { copyright } = e;
                              return (
                                <Image
                                  key={index + 1}
                                  borderRadius="lg"
                                  src={
                                    "https://images.unsplash.com/photo-1499951360447-b19be8fe80f5?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=800&q=80"
                                  }
                                  alt={copyright}
                                  objectFit="contain"
                                />
                              );
                            })}
                        </Link>
                      </Box>
                      <Box
                        zIndex="1"
                        width="100%"
                        position="absolute"
                        height="100%"
                      >
                        <Box
                          bgGradient={useColorModeValue(
                            "radial(orange.600 1px, transparent 1px)",
                            "radial(orange.300 1px, transparent 1px)"
                          )}
                          backgroundSize="20px 20px"
                          opacity="0.4"
                          height="100%"
                        />
                      </Box>
                    </Box>
                    <Box
                      display="flex"
                      flex="1"
                      flexDirection="column"
                      justifyContent="center"
                      marginTop={{ base: "3", sm: "0" }}
                    >
                      <HStack spacing={2} marginTop={4}>
                        <Tag size={"md"} variant="solid" colorScheme="orange">
                          {subsection}
                        </Tag>
                      </HStack>
                      <Heading marginTop="1">
                        <Link
                          href={`most-popular/` + id}
                          textDecoration="none"
                          _hover={{ textDecoration: "none" }}
                        >
                          {title}
                        </Link>
                      </Heading>
                      <Text
                        as="p"
                        marginTop="2"
                        color={useColorModeValue("gray.700", "gray.200")}
                        fontSize="lg"
                      >
                        {abstract}
                      </Text>
                      <BlogAuthor name={byline} date={published_date} />
                    </Box>
                  </Box>
                );
              })}
          </SimpleGrid>
        </InfiniteScroll>

        <Center mt={5}>
          <Button onClick={loadMore} justifyItems={"center"} colorScheme="blue">
            Load More
          </Button>
        </Center>
      </Container>
    </>
  );
};

interface BlogAuthorProps {
  date: string;
  name: string;
}

export const BlogAuthor: React.FC<BlogAuthorProps> = (props) => {
  return (
    <HStack marginTop="2" spacing="2" display="flex" alignItems="center">
      <Image
        borderRadius="full"
        boxSize="40px"
        src="https://100k-faces.glitch.me/random-image"
        alt={`Avatar of ${props.name}`}
      />
      <Text fontWeight="medium">{props.name}</Text>
      <Text>{props.date}</Text>
    </HStack>
  );
};

export default Home;
