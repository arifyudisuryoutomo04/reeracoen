import {
  Box,
  chakra,
  Container,
  Stack,
  Text,
  Image,
  Flex,
  VStack,
  Button,
  Heading,
  SimpleGrid,
  StackDivider,
  useColorModeValue,
  VisuallyHidden,
  List,
  ListItem
} from "@chakra-ui/react";
import { MdLocalShipping } from "react-icons/md";
import React, { useState, useEffect } from "react";
import axios from "axios";
import { prototype } from "events";

export default function Simple({ Most: any }) {
  const [next, setNext] = useState([1 && 7 && 30]);
  const [Most, setMost] = useState([]);

  useEffect(() => {
    async function fetchMost() {
      const res = await axios.get(
        `https://api.nytimes.com/svc/mostpopular/v2/emailed/${next}.json?api-key=${process.env.customKey}`
      );
      setMost(res.data.results);
    }
    fetchMost();
    console.log(Most);
  }, []);

  return (
    <Container maxW={"7xl"}>
      <SimpleGrid
        columns={{ base: 1, lg: 2 }}
        spacing={{ base: 8, md: 10 }}
        py={{ base: 18, md: 24 }}
      >
        <Flex>
          <Image
            rounded={"md"}
            alt={"product image"}
            src={
              "https://images.unsplash.com/photo-1596516109370-29001ec8ec36?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyODE1MDl8MHwxfGFsbHx8fHx8fHx8fDE2Mzg5MzY2MzE&ixlib=rb-1.2.1&q=80&w=1080"
            }
            fit={"cover"}
            align={"center"}
            w={"100%"}
            h={{ base: "100%", sm: "400px", lg: "500px" }}
          />
        </Flex>
        <Stack spacing={{ base: 6, md: 10 }}>
          <Box as={"header"}>
            {/* {Most.map((x) => ( */}
            <Heading
              lineHeight={1.1}
              fontWeight={600}
              fontSize={{ base: "2xl", sm: "4xl", lg: "5xl" }}
            >
              {Most.title}
            </Heading>
            {/* ))} */}

            <Text
              color={useColorModeValue("gray.900", "gray.400")}
              fontWeight={300}
              fontSize={"2xl"}
            >
              {Most.type}
            </Text>
          </Box>

          <Stack spacing={{ base: 4, sm: 6 }} direction={"column"}>
            <VStack spacing={{ base: 4, sm: 6 }}>
              <Text fontSize={"lg"}>{Most.abstract}</Text>
            </VStack>
          </Stack>

          <Stack direction="row" alignItems="center" justifyContent={"center"}>
            <MdLocalShipping />
            <Text>{Most.updated}</Text>
          </Stack>
        </Stack>
      </SimpleGrid>
    </Container>
  );
}
