<div align="center">
  <a href="/reeracoen.jpg">
    <img src="/reeracoen.jpg" alt="reeracoen" width="200px" height="200px">
  </a>
</div>

## Getting Started Frontend

First, install all needed dependencies:

```bash
cd frontend
```

```bash
npm install
```

Then, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## More

To learn more about this project, take a look at the following resources:

- [JavaScript ES6](https://www.w3schools.com/Js/js_es6.asp) - learn about JavaScript ES6 before continue to React
- [TypeScript](https://www.typescriptlang.org/) - this project uses TypeScript as main language
- [React](https://reactjs.org/) - learn about React and React Features (e.g. React Hooks)
- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
- [Chakra UI](https://chakra-ui.com/) - learn Chakra UI. This project uses Chakra UI as UI Library
